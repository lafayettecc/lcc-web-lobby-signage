<?php include 'lib.php'; ?>
<?php
if (isset($_GET['fancy'])) $fancy = $_GET['fancy'];
else $fancy=0;

if (isset($_GET['effect'])) $effect = $_GET['effect'];
else $effect='fade';
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="lib.js"></script>
<link href='//fonts.googleapis.com/css?family=Roboto:100,400,700,900,700italic|Julius+Sans+One' rel='stylesheet' type='text/css'>
<style>
	* {margin:0;padding:0;border:0;box-sizing:border-box;}
	body {width: 1920px; height: 1080px;margin:0; padding:0; background-color: black;position:relative;overflow:hidden;}
	window {width: 1920px; height: 1080px;margin:0; padding:0; background-color: red;position:absolute;}
	window, placeholder {transform-origin: 0px 0px;transform:scale3d(1,1,1);}
	content-full {width: 1920px; height: 1080px; background-color: blue;position:absolute;top:0; left:0;}
	content-left {width: 1440px; height: 1080px; background-color: white;position:absolute;top:0;left:0;}
	stoplight {width: 480px; border-left:10px solid black; height: 1080px; background-color: #dda;position:absolute;right:0px;top:0;z-index:99;}
	ticker {z-index:90;color:white;}

	stoplight {background:#000;}
	stoplight img.stoplight {width:480px;position:absolute;top:0;left:0;transition:opacity 1s;}
	img.stoplight.off {opacity:0;}
/*	img.stoplight.on {opacity:1;}*/

	light, red, yellow, green
	{
		width: 270px;
		height: 270px;
		border-radius: 150px;
		border:15px solid black;
		background-color: black;
		position: absolute;
		left:83px;
	}
	/*light {width:198px;height:198px;}*/
	light {height:290px;}
	#light-1 {top: 30px;}
	#light-2 {top: 375px;}
	#light-3 {top: 720px;}
	red {background-color: #f00;top: 45px;}
	yellow {background-color: #ff0;top: 390px;}
	green {background-color: #10f010;;top: 735px;}
	.light {opacity: 0.1;}

	/* with gradients */
	red{background-image: -webkit-linear-gradient(bottom, rgb(140,10,10) 4%, rgb(255,0,0) 52%, rgb(217,7,7) 76%);}
	yellow{background-image: -webkit-linear-gradient(bottom, rgb(163,155,3) 4%, rgb(255,242,0) 52%, rgb(240,228,0) 76%);}
	green{background-image: -webkit-linear-gradient(bottom, rgb(18,163,18) 4%, rgb(16,240,16) 52%, rgb(9,224,9) 76%); }
	ticker {width:1440px; height:100px;position:absolute;bottom:-1px;left:0;background:black;opacity:.9;z-index:11;}
	ticker.red {background:#f00;}
	ticker.yellow {background-color:#FFF200;color:black;}
	ticker.green {background-color:#009100;}
	progress {position:absolute; bottom:0;left:0;width:100%;height:5px;color:#fff; opacity:.5}
	room_status {position:absolute;right:15px;text-align:right;line-height:72pt;min-width:1000px;}
	room_status {font-family:Roboto;text-transform:uppercase;font-weight:100;font-size:40px;}
	room_status strong{font-weight:900;font-size:1.3em;}
	clock {position:absolute;left:15px;text-align:left;font-family:'Roboto', Impact, sans-serif;font-weight:700;font-size: 58pt;}
	slideshow {position:absolute;top:0;left:0;width:100%;height:1080px;overflow:hidden;}
	slide {position:absolute; top:0;left:0;min-width:100%;min-height:1080px;overflow:hidden;}
	slide img {width:100%;min-height:1080px;}
	secured {position:absolute;top:0;left:0;width:100%;height:100%;z-index:10;background:red;text-align:center;color:white;}
	secured div {display:table;height:100%;}
	secured div span {display:table-cell;vertical-align:middle;padding:150px;font-family:"Julius Sans One";font-size:90pt;}
	placeholder {font-family:"Julius Sans One";font-size:44pt;position:absolute;top:0;left:0;width:100%;height:100%;z-index:999;background:black;text-align:center;line-height:1080px;color:white;}
	#throbber {display:block;height:64px;}

	logo {z-index:10;position:absolute;top:10px;left:20px;}
	logo img {width:300px;height:auto;}
/*
	slideprogressbar {z-index:99;background:transparent;width:100%;padding:0;margin:0;height:3px;position:fixed;top:0;;left:0;}
	slideprogress {z-index:99;background:rgba(255,255,255,.5);width:100px;margin:0;padding:0;height:3px;position:absolute;}
*/
	
	/* ATTEMPT TO ACTIVATE 3D */
	slide {
		-webkit-transform: translateZ(0);
		-moz-transform: translateZ(0);
		-ms-transform: translateZ(0);
		-o-transform: translateZ(0);
		transform: translateZ(0);

	    -webkit-backface-visibility: hidden;
	    -moz-backface-visibility: hidden;
	    -ms-backface-visibility: hidden;
	    backface-visibility: hidden;

	    -webkit-perspective: 1000;
	    -moz-perspective: 1000;
	    -ms-perspective: 1000;
	    perspective: 1000;
	}
	
	.growable img {transition: all 7s ease-in-out;}
	.growable.growing img {transform: scale(1.03);}
	
</style>
</head>
<body>
<window>
<placeholder><div id="throbber"></div>Loading...</placeholder>
<!-- <slideprogressbar>
	<slideprogress></slideprogress>
</slideprogressbar> -->
<content-full>content</content-full>
<content-left>
<logo><img src="//lafayettecc.org/kidopolis/images/kidopolis_logo.png" /></logo>
<secured>
	<div><span>Kidopolis is now in session.</span></div>
</secured>
<slideshow>

<?php foreach (get_ads('kidopolis') as $ad): ?>

	<slide class="growable"><img src="<?php print $ad; ?>" /></slide>

<?php endforeach; ?>

</slideshow>
</content-left>
<stoplight>
<img class="stoplight stoplight-red" id="red" src="red.jpg" />
<img class="stoplight stoplight-yellow off" id="yellow" src="yellow.jpg" />
<img class="stoplight stoplight-green off" id="green" src="green.jpg" />
<!--
<light id="light-1">&nbsp;</light>
<light id="light-2">&nbsp;</light>
<light id="light-3">&nbsp;</light>
<red class="off light">&nbsp;</red>
<yellow class="off light">&nbsp;</yellow>
<green class="off light">&nbsp;</green>
 -->
</stoplight>
<ticker>
<!-- <progress>&nbsp;</progress> -->
<clock>9:00 am</clock>
<room_status>Kidopolis rooms will open in 30 minutes.</room_status>
</ticker>
</window>
</body>
<script>

// requestAnimationFrame() shim by Paul Irish
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
window.requestAnimFrame = (function() {
	return  window.requestAnimationFrame       ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			window.oRequestAnimationFrame      ||
			window.msRequestAnimationFrame     ||
			function(/* function */ callback, /* DOMElement */ element){
				window.setTimeout(callback, 1000 / 60);
			};
})();

var today=new Date();
today.setHours(0)
today.setMinutes(0)
today.setSeconds(0);

<?php if ($_GET['test'] == 1): ?>

page_load_time = new Date();
page_load_ms = page_load_time.getTime()

<?php endif; ?>

var throbber;
var opacity=1;
var current_color='none';
var slide_interval = 6.5; // in seconds
var current_slide=0;
var slideshow_started=0;
var last_slide_switch = 0;
var slideshow_timer;
var clock_timer;
var do_fancy = <?php if ($fancy) echo 1; else echo 0; ?>;
var do_grow = do_fancy;
var transition_effect = '<?php echo $effect ?>';
var room_status = 0;


// service time settings
var service_times=new Array(
	new Date(today.getYear(), today.getMonth(), today.getDate(), 9, 0, 0, 0),
	new Date(today.getYear(), today.getMonth(), today.getDate(), 10, 45, 0, 0)
);

// room status options
// closed, almost, open, closing, secured, pickup :: these loop from open to pickup with open taking priority
var opens_how_early = 15;
var closing_warning = 5;
var registration_grace_time = 10;
var service_length = 70;
var pickup_duration = 20;

// set up the default room status
var status_options = {
	'opening': {priority: 1, status: 'opening', light: 'red',    text: 'Kidopolis rooms will reopen in <strong>1 minute.</strong>'},
	'open'   : {priority: 3, status: 'open',    light: 'green',  text: 'Kidopolis rooms are <strong>open for check-in.</strong>'},
	'closing': {priority: 4, status: 'closing', light: 'yellow', text: '<strong>Check-in will close soon.</strong>'},
	'secured': {priority: 9, status: 'secured', light: 'red',    text: 'Kidopolis in session. <strong>Check-in is closed.</strong>'},
	'pickup' : {priority: 2, status: 'pickup',  light: 'yellow', text: '<strong>Caution!</strong> Kidopolis pick-up traffic ahead.'},
	'closed' : {priority: 1, status: 'closed',  light: 'red',    text: 'Kidopolis is now <strong>closed.</strong>'},
}


// set jquery animation speed
jQuery.fx.interval = <?php if ($fancy) echo 10; else echo 100; ?>;

function pulse_light()
{
	$('.on').fadeTo(500,.2).fadeTo(500,1);
	setTimeout(pulse_light, 2000);
}

function turn_on(light)
{
	if (current_color == light) return;

	// update the ticker color
	$('ticker').removeClass('red');
	$('ticker').removeClass('green');
	$('ticker').removeClass('yellow');
	$('ticker').addClass(light);

	// turn off all the lights
	$('.light,.stoplight').removeClass('on');
	$('.light,.stoplight').addClass('off');

	// turn on the one light matching this light's tag
	$(light).addClass('on');
	$(light).removeClass('off');
	var light_image = $('#' + light)
	light_image.removeClass('off')

	// handle fading of the lights
	// $('.on').fadeTo(1000,1);
	// $('.off').fadeTo(1000,0.1);

	// update the current_color
	current_color = light;
}
function adjust_opacity()
{
	opacity = (opacity + 1) % 10;
	$('.off').fadeTo(1000, opacity / 10);
	$('#adjust').html(opacity);
}
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function check_lights(date)
{
	s = date.getSeconds()
	i = Math.floor(s/10) %3;
	colors = new Array('red','yellow','green');
	if (colors[i] != current_color) turn_on(colors[i]);
}

function do_slideshow()
{
	// setup next slideshow time
	window.slideshowTimer = window.setTimeout(do_slideshow, slide_interval*1000);
	
	if (room_status.status == 'secured') return;
	

	if ( ! slideshow_started )
	{
		// hides all slides to start either by "hiding" them or by pushing them to the right
		if (do_fancy && transition_effect == 'slide') $('slide').css('left', '1920px');
		else $('slide').hide();

		$($('slide').get(0)).show().css('left', '0');
		slideshow_started = 1;
		current_slide = 0;
	}
	else
	{
		slides = $('slide');
		next_slide = (current_slide + 1) % slides.length;

		// method =================================
		// put current slide at z-index of 1
		// put the next slide at a z-index of 2
		// reveal the new slide with an animation
		// put the old slide at z-index 0 and hide.
		var current = $(slides.get(current_slide));
		var next = $(slides.get(next_slide))

		// adjust ordering
		current.css('z-index',1)
		next.css('z-index',2)
		
		// if the current slide is scaled, make the next one scaled before transitioning to it
		if (do_grow)
		{
			if (current.hasClass('growing'))
			{
				next.addClass('growing');
			}
			else
			{
				next.removeClass('growing');
			}
		}
		
		// reveal the new slide
		if (do_fancy)
		{
			if (transition_effect == 'fade') next.fadeIn(1000, function(){current.hide(); current.css('z-index', 0)});
			else next.animate({left:0}, 1000, 'swing', function(){current.css({'z-index': 0, 'left':'1920px'})});
			// if (do_grow) setTimeout(() => {next.toggleClass('growing')}, 1000);
			if (do_grow) next.toggleClass('growing');
		}
		else next.show(0, function(){current.hide(); current.css({'z-index': 0})});

		// if (next_slide == 0) get_serve_data();
		current_slide = next_slide;
	}
}


function old_do_slideshow()
{
	if ( ! slideshow_started )
	{
		// to initialize the slideshow
		// hide all slides
		// show slide 0
		$('slide').hide();
		$($('slide').get(0)).show();
		slideshow_started = 1;
		current_slide = 0;
	}
	else
	{
		slides = $('slide');
		var next_slide = current_slide + 1;
		if (next_slide == slides.length) next_slide=0;

		// hide the current slide and show the next slide
		if(do_fancy)
		{
			$(slides.get(current_slide)).fadeOut(2000);
			$(slides.get(next_slide)).fadeIn(2000);
		}
		else
		{
			$(slides.get(current_slide)).hide();
			$(slides.get(next_slide)).show();
		}

		// update the current_slide
		current_slide = next_slide;
	}

	last_slide_switch = Date.now();
}

function do_clock()
{
	// call this function again later
	window.clockTimer = window.setTimeout(do_clock, 1000);
	
	// first, we compute the clock and show it
	var d = new Date();
	

	<?php if ($_GET['test']==1): ?>

	elapsed_time = 2 * (d.getTime() - page_load_ms);
	fake_clock = new Date();
	fake_clock.setHours(8);
	fake_clock.setMinutes(26);
	d.setTime( (fake_clock.getTime() + elapsed_time * 60 ));

	<?php endif; ?>

	clock=get_clock(d);
	$('clock').html(clock);

	// next we compute the room open or closed status
	var time_diffs = new Array();
	for (i in service_times)
	{
		var service_minutes = service_times[i].getHours() * 60 + service_times[i].getMinutes();
		var close_minutes = service_minutes + registration_grace_time;
		var now_minutes = d.getHours() * 60 + d.getMinutes();
		var diff = service_minutes - now_minutes;
		time_diffs.push({diff: diff, close_minutes: close_minutes, service_minutes:service_minutes})
	}

	// now figure out the kidopolis status from the service time diffs we just computed

	// if the first diff is positive, we are before the first service start
	// if the last diff is negative, we are after the last service start
	var first_diff = time_diffs[0].diff;
	var last_diff = time_diffs[time_diffs.length - 1].diff;

	var need_room_status = true;
	// preparing to open the rooms
	if (first_diff > opens_how_early)
	{
		room_status = status_options['opening'];
		room_status.remaining = first_diff - opens_how_early;
		need_room_status = false;
	}

	// after the final service of the day, close the rooms
	else if (last_diff + service_length + pickup_duration < 0)
	{
		// kidopolis is closed
		room_status = status_options['closed'];
		need_room_status = false;
	}

	// compute room status from the different time diffs
	// to do that, we check each service time to determine
	// if the room status for that service supersedes the
	// previously set room status
	if (need_room_status)
	{
		room_status = status_options['closed'];
		for (i in time_diffs)
		{
			var service_diff = time_diffs[i].diff;
			var service_status = '';
			var close_minutes = 0;

			if (service_diff > opens_how_early) service_status = 'opening';
			if (service_diff <= opens_how_early) service_status = 'open';
			if (service_diff - closing_warning + registration_grace_time <= 0 ) service_status = 'closing';
			if (service_diff + registration_grace_time <= 0)
			{
				service_status = 'secured';
				close_minutes = time_diffs[i].close_minutes;
			}
			if (service_diff + service_length <= 0) service_status = 'pickup';
			if (service_diff + service_length + pickup_duration <= 0) service_status = 'closed';

			// select status according to priorities
			if (status_options[service_status].priority > status_options[room_status.status].priority)
				room_status = status_options[service_status];

			// store room remaining minutes if the status is "opening"
			if (room_status.status == 'opening') room_status.remaining = service_diff-opens_how_early;
			if (close_minutes > 0) room_status.close_minutes = close_minutes;
		}
	}

	if (room_status.status == 'opening')
	{
		if (room_status.remaining > 1)
			room_status.text = 'Kidopolis rooms will reopen in <strong>' + room_status.remaining + ' minutes.</strong>';
		else
			room_status.text = 'Kidopolis rooms will reopen in <strong>1 minute.</strong>';
	}


	// for (i in time_diffs){
	//
	//
	// 	var diff = time_diffs[i];
	// 	console.log(diff);
	//
	// 	// it's 100 minutes after the service start time, label Kidopolis closed.
	// 	if (diff < -100) room_status = {status: 'closed', light: 'red', text: 'Kidopolis is now <strong>closed.</strong>'}
	//
	// 	// it's 75 minutes after the service start time, parents should pick up their kids with care
	// 	else if (diff <= -75) room_status = {status: 'pickup', light: 'yellow', text: '<strong>Caution!</strong> Kidopolis pick-up traffic ahead.'}
	//
	// 	// registration grace period has elapsed, close registration
	// 	else if (diff <= (-1) * (grace_time)) room_status = {status: 'secured', light: 'red', text: 'Kidopolis in session. <strong>Check-in is closed.</strong>'};
	//
	// 	// it's 5 minutes until the grace period elapses, warn of closing registration
	// 	else if (diff <= (-1) * (grace_time - 5)) room_status = {status: 'closing', light: 'yellow', text: '<strong>Check-in will close soon.</strong>'}
	//
	// 	// it's 15 minutes BEFORE the service start time, open registration & check-in
	// 	else if (diff <= 15) room_status = {status: 'open', light: 'green', text: 'Kidopolis rooms are <strong>open for check-in.</strong>'};
	//
	// 	// definitely before or during this service, so we break and don't consider the next service time
	// 	if (diff >= -75) break;
	// }

	turn_on(room_status.light);
	$('room_status').html(room_status.text);

	// if kidopolis is secured, show the overlay instead of the slideshow
	if (room_status.status == 'secured')
	{
		var close_minutes = room_status.close_minutes;
		var start_hour = Math.floor(service_minutes / 60);
		var start_minutes = ('0' + (service_minutes % 60)).slice(-2);
		var close_hour = Math.floor(close_minutes / 60);
		close_minutes = ('0' + (close_minutes % 60)).slice(-2);

		$('secured').show();
		$('secured div span').html("Kidopolis is in Session.<br /><br />Check-in closed at " + close_hour + ":" + close_minutes + "am");
		// set_progress(0);
	}
	else
	{
		$('secured').hide();
		// var slide_elapsed = Date.now() - last_slide_switch;
		// if ( slide_elapsed > slide_interval ) do_slideshow();
		// else set_progress(slide_elapsed / slide_interval * 1440);
	}
}

function set_progress(n)
{
	$('progress').width(n);
	// $('slideprogress').width(n);
}

function stop()
{
	clearTimeout(slideshow_timer);
	clearTimeout(clock_timer);
}

function start()
{
	do_clock();
	do_slideshow();
}

$(window).load(function(){
	// do_clock();
	// do_slideshow();
	start();
	throbber.stop();
	$('placeholder').hide();
})

$(document).ready(function(){
	throbber = new Throbber('throbber');
	throbber.options.spokes = 20;
	throbber.options.speedMS = 200;
	throbber.options.style = 'balls';
	throbber.throb();
})


function Throbber(containerId) {
  this.options = {
    speedMS: 100,
    center: 4,
    thickness: 3,
    spokes:8,
    color: [0,0,0],
    style: "line" //set to "balls" for a different style of throbber
  };
  this.t = document.getElementById(containerId);
  this.c = document.createElement('canvas');
  this.c.width = this.t.offsetWidth;
  this.c.height = this.t.offsetHeight;
  this.t.appendChild(this.c);
  this.throb = function() {
    var ctx = this.c.getContext("2d");
    ctx.translate(this.c.width/2, this.c.height/2);
    var w = Math.floor(Math.min(this.c.width,this.c.height)/2);
    var self = this;
    var o = self.options;
    var draw = function() {
      ctx.clearRect(-self.c.width/2,-self.c.height/2,self.c.width,self.c.height)
      ctx.restore();
      ctx.shadowOffsetX = ctx.shadowOffsetY = 1;
        ctx.shadowBlur = 2;
        ctx.shadowColor = "rgba(220, 220, 220, 0.5)";
        for (var i = 0; i < o.spokes; i++) {
        r = 255-Math.floor((255-o.color[0]) / o.spokes * i);
        g = 255-Math.floor((255-o.color[1]) / o.spokes * i);
        b = 255-Math.floor((255-o.color[2]) / o.spokes * i);
          ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
        if(o.style == "balls") {
          ctx.beginPath();
          ctx.moveTo(w,0)
          ctx.arc(w-Math.floor(Math.PI*2*w/o.spokes/3),0,Math.floor(Math.PI*2*w/o.spokes/3),0,Math.PI*2,true);
          ctx.fill();
        } else { ctx.fillRect(o.center, -Math.floor(o.thickness/2), w-o.center, o.thickness); }
        ctx.rotate(Math.PI/(o.spokes/2))
        if(i == 0) { ctx.save(); }
      }
    };
    draw();
    this.timer = setInterval(draw,this.options.speedMS);
  };
  this.stop = function() {
    clearInterval(this.timer);
    this.c.getContext("2d").clearRect(-this.c.width/2,-this.c.height/2,this.c.width,this.c.height)
  };
};

function do_resize()
{
	var w = window.innerWidth;
	var s = w / 1920;
	$('window').css('transform','scale3d(' + s + ',' + s + ',' + s + ')');
	$('placeholder').css('transform','scale3d(' + s + ',' + s + ',' + s + ')');
}
$(window).load(do_resize)
$(window).resize(do_resize)
</script>

</html>
