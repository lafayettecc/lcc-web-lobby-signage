# Readme

This repo is a very simple digital signage server.

## Server Installation
1. Put it on a web server capable of running php.
2. rename conf.php.dist to conf.php
3. edit conf.php to create a username and password for the admin, and to set URL settings
4. edit conf.js to update URL settings for javascript functions
4. visit http://domain/path/admin.php in your browser

## Client Installation
1. Any device with a fullscreen browser can be a client. For Android devices, the Fully browser app is quite good.
2. I sell an app for Android devices that also enables automatic switching to live-streaming video.

for help, contact jeffmikels@gmail.com
