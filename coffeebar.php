<?php include 'lib.php'; ?>
<?php
if (isset($_GET['fancy'])) $fancy = $_GET['fancy'];
else $fancy=0;

if (isset($_GET['effect'])) $effect = $_GET['effect'];
else $effect='slide';
?>
<!DOCTYPE html>
<html>
<head>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="lib.js"></script>
<link href='//fonts.googleapis.com/css?family=Roboto:100,400,700,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:100,400,700,900,700italic|Julius+Sans+One' rel='stylesheet' type='text/css'>
<!-- VIDEOJS -->
<link href="https://vjs.zencdn.net/6.2.4/video-js.css" rel="stylesheet">

<style>
	* {margin:0;padding:0;border:0;}
	body {width: 1920px; height: 1080px;margin:0; padding:0; background-color: black;position:relative;overflow:hidden;}
	window {width: 1920px; height: 1080px;margin:0; padding:0; background-color: red;position:absolute;top:0;left:0;}
	window, placeholder {transform-origin: 0px 0px;transform:scale3d(1,1,1);}
	content-full {width: 1920px; height: 1080px; background-color: black;position:absolute;top:0; left:0;}
	content-left {width: 1440px; height: 1080px; background-color: white;position:absolute;top:0;left:0;}
	stoplight {width: 480px; border-left:10px solid black; height: 1080px; background-color: #dda;position:absolute;right:0px;top:0;}
	light, red, yellow, green
	{
		width: 270px;
		height: 270px;
		border-radius: 150px;
		border:15px solid black;
		background-color: black;
		position: absolute;
		left:83px;
	}
	/*light {width:198px;height:198px;}*/
	light {height:290px;}
	#light-1 {top: 30px;}
	#light-2 {top: 375px;}
	#light-3 {top: 720px;}
	red {background-color: #f00;top: 45px;}
	yellow {background-color: #ff0;top: 390px;}
	green {background-color: #10f010;;top: 735px;}
	.light {opacity: 0.1;}

	/* with gradients */
	red{background-image: -webkit-linear-gradient(bottom, rgb(140,10,10) 4%, rgb(255,0,0) 52%, rgb(217,7,7) 76%);}
	yellow{background-image: -webkit-linear-gradient(bottom, rgb(163,155,3) 4%, rgb(255,242,0) 52%, rgb(240,228,0) 76%);}
	green{background-image: -webkit-linear-gradient(bottom, rgb(18,163,18) 4%, rgb(16,240,16) 52%, rgb(9,224,9) 76%); }
	ticker {width:100%; height:100px;position:absolute;top:0;left:0;background:black;opacity:.5;}
	clock_status {position:absolute;right:15px;text-align:right;color:white;font-family:Roboto, serif;font-size:44pt;line-height:72pt;min-width:1000px;}
	clock_status{font-family:Roboto;text-transform:uppercase;font-weight:900;font-size:60px;}
	clock {position:absolute;left:15px;text-align:left;color:white;font-family:'Roboto', Impact, sans-serif;font-weight:700;font-size: 58pt;}
	slideshow {position:absolute;top:0;left:0;width:100%;height:1080px;overflow:hidden;}
	slide {position:absolute; top:0px;left:0;min-width:100%;min-height:1080px;overflow:hidden;}
	slide img {width:100%;min-height:1080px;}
	progress {position:absolute; bottom:0;left:0;width:100%;height:5px;color:#fff; opacity:.5}
	overlay {font-family:"Roboto";color:white;font-size:1000px;text-shadow:20px 20px 20px #000;font-weight:700;width:1920px;height:1080px;display:none;position:absolute;top:0;left:0;background:#003;line-height:1080px;text-align:center;}
	overlay small {font-size:60px;}
	placeholder {font-family:"Julius Sans One";font-size:44pt;position:absolute;top:0;left:0;width:100%;height:100%;background:black;text-align:center;line-height:1080px;color:white;}
	
	#throbber {display:block;height:64px;}
	.serve_caption {bottom:0px;position:absolute;font-size:40px;font-family:Roboto;color:white;padding:16px;box-sizing:border-box;background:black;width:100%;}
	.serve_caption {background-color:rgba(0,0,0,.5);}
	.stats {float:right;font-size:.85em;}

	/* HANDLE OVERLAYS */
	.serve_caption {z-index:10;}
	overlay        {z-index:20;}
	ticker         {z-index:90;}
	#player        {z-index:89;}
	placeholder    {z-index:99;}

	/* ATTEMPT TO ACTIVATE 3D */
	slide, #throbber, video {
		-webkit-transform: translateZ(0);
		-moz-transform: translateZ(0);
		-ms-transform: translateZ(0);
		-o-transform: translateZ(0);
		transform: translateZ(0);

	    -webkit-backface-visibility: hidden;
	    -moz-backface-visibility: hidden;
	    -ms-backface-visibility: hidden;
	    backface-visibility: hidden;

	    -webkit-perspective: 1000;
	    -moz-perspective: 1000;
	    -ms-perspective: 1000;
	    perspective: 1000;
	}
	
	.growable img {transition: all 7s ease-in-out;}
	.growable.growing img {transform: scale(1.03);}
	
	#player {width:100%;height:auto;margin:auto;background:black;position:absolute;top:0;left:0;display:none;}
</style>
</head>
<body>
<window>
<placeholder><div id="throbber"></div>Loading...</placeholder>
<content-full>
<slideshow>
	<!-- The Coffeebar sign alternates between an ad and the coffee menu -->
	<!-- The menu is assumed to be the first ad image. -->
	<!-- <slide id="menu"><img src="coffeebar-menu.jpg" alt="" /></slide> -->
	<?php foreach (get_ads('coffeebar') as $ad): ?>
	<slide class="growable"><img src="<?php echo $ad; ?>" /></slide>
	<?php endforeach; ?>
</slideshow>
<video class="video-js" id="player"></video>
<menu></menu>
<ticker>
<progress>&nbsp;</progress>
<clock>9:00 am</clock>
<clock_status>Kidopolis rooms will open in 30 minutes.</clock_status>
</ticker>
</content-full>
</window>
</body>
<script>
var today=new Date();
today.setHours(0)
today.setMinutes(0)
today.setSeconds(0);

<?php if ($_GET['test'] == 1): ?>

page_load_time = new Date();
page_load_ms = page_load_time.getTime()

<?php endif; ?>

var doing_menu=1;

var opacity=1;
var current_color='none';
var slide_interval = 7;
var current_slide=0;
var slideshow_started=0;
var service_times=new Array(
	new Date(today.getYear(), today.getMonth(), today.getDate(), 9, 0, 0, 0),
	new Date(today.getYear(), today.getMonth(), today.getDate(), 10, 45, 0, 0)
);
var is_final_countdown = 0;
var do_fancy = <?php if ($fancy) echo 1; else echo 0; ?>;
var transition_effect = '<?php echo $effect ?>';
var do_grow = <?php if ($fancy) echo 1; else echo 0; ?>;

// set jquery animation speed
jQuery.fx.interval = <?php if ($fancy) echo 13; else echo 50; ?>;

function pulse_light()
{
	$('.on').fadeTo(500,.2).fadeTo(500,1);
	setTimeout(pulse_light, 2000);
}

function turn_on(light)
{
	if (current_color == light) return;
	$('.light').removeClass('on');
	$('.light').addClass('off');
	$(light).addClass('on');
	$(light).removeClass('off');
	$('.on').fadeTo(1000,1);
	$('.off').fadeTo(1000,0.1);
	current_color = light;
}
function adjust_opacity()
{
	opacity = (opacity + 1) % 10;
	$('.off').fadeTo(1000, opacity / 10);
	$('#adjust').html(opacity);
}
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function check_lights(date)
{
	s = date.getSeconds()
	i = Math.floor(s/10) %3;
	colors = new Array('red','yellow','green');
	if (colors[i] != current_color) turn_on(colors[i]);
}
function do_slideshow()
{
	// check for a livestream
	check_livestream()
	
	
	window.slide_timer = window.setTimeout(do_slideshow, slide_interval*1000);
	if ( is_final_countdown ) return;

	if ( ! slideshow_started )
	{
		// hides all slides to start either by "hiding" them or by pushing them to the right
		if (do_fancy && transition_effect == 'slide') $('slide').css('left', '1920px');
		else $('slide').hide();

		$($('slide').get(0)).show().css('left', '0');
		slideshow_started = 1;
		current_slide = 0;
	}
	else
	{
		slides = $('slide');

		// if we are currently showing the menu, make sure the next slide is not a menu slide
		// if we are not showing the menu, make sure the next slide is the menu slide
		if (doing_menu == 1)
		{
			next_slide = (current_slide + 1) % slides.length;

			// ignore the first slide... that's the menu slide
			if (next_slide == 0) next_slide = 1;
			
			// if we are doing the menu slide then "current_slide"
			// will be the number of the last shown slide to keep
			// our counts correct, but the real current slide is 0
			var current = $(slides.get(0));
			var next = $(slides.get(next_slide))

			current_slide = next_slide;

			// we are no longer doing the menu slide
			doing_menu = 0;
		}
		else
		{
			var current = $(slides.get(current_slide));
			var next = $(slides.get(0))

			// now we are doing the menu slide
			doing_menu = 1;
		}


		// method =================================
		// put current slide at z-index of 1
		// put the next slide at a z-index of 2
		// reveal the new slide with an animation
		// put the old slide at z-index 0 and hide.

		// adjust ordering
		current.css('z-index',1)
		next.css('z-index',2)
		
		// reveal the new slide
		if (do_fancy)
		{
			if (transition_effect == 'fade') next.fadeIn(1000, function(){current.hide(); current.css('z-index', 0)});
			else next.animate({left:0}, 1000, 'swing', function(){current.css({'z-index': 0, 'left':'1920px'})});
			if (do_grow) next.toggleClass('growing');
		}
		else next.show(0, function(){current.hide(); current.css({'z-index': 0});});
		
		
		// refresh the advertisement slides
		if (current_slide == 1) get_serve_data();

	}
}

function do_slideshow_restart()
{
	get_serve_data();
	current_slide=0;
}

function do_clock()
{
	is_final_countdown = 0;
	window.clock_timer = window.setTimeout(do_clock, 1000);
	
	// first, we compute the clock and show it
	var d = new Date();

	<?php if ($_GET['test']==1): ?>

	elapsed_time = d.getTime() - page_load_ms;
	fake_clock = new Date();

	// force it to Sunday morning
	fake_clock.setDate( fake_clock.getDate() - fake_clock.getDay() );
	fake_clock.setHours(10);
	fake_clock.setMinutes(43);
	fake_clock.setSeconds(0);
	d.setTime( (fake_clock.getTime() + elapsed_time * 1 ));

	<?php endif; ?>

	//check_lights(d);
	clock=get_clock(d);
	$('clock').html(clock);


	// compute the next service time
	next_service = 0;
	possible_services = new Array();

	for (i in service_times)
	{
		possible_service = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay(), service_times[i].getHours(), service_times[i].getMinutes(), 0, 0);
		if (possible_service.getTime() < (d.getTime() - 45 * 60 * 1000)) possible_service.setDate(possible_service.getDate() + 7);
		possible_services.push(possible_service);
 		//alert(possible_service.toString());
	}

	// get the earliest service from the list of possible_services
	next_service_timestamp = Math.min.apply( null, possible_services );

	diff = new Date(next_service_timestamp - d.getTime());


	if ( (diff <= 1) && (Math.abs(diff) < (45 * 60 * 1000)) )
	{
		clock_string = 'The party\'s inside, please be quiet out here!';
		$('overlay').hide();
	}
	else
	{
		// FROM THIS POINT, the diff object holds the time difference, but only in UTC time
		if (diff < 100*1000)
		{
			is_final_countdown = 1;
			do_final_countdown(diff.getTime());
			clock_string = 'Please enter the auditorium. It\'s time!';
		}
		else
		{
			$('overlay').hide();
			days_until = diff.getUTCDate() - 1;
			hours_until = diff.getUTCHours();
			minutes_until = diff.getUTCMinutes();
			seconds_until = diff.getUTCSeconds();

			days_string = '';
			if (days_until) days_string = days_until + ' days, ';

			hours_string = '';
			if (days_until || hours_until) hours_string = hours_until + ' hours and ';

			minutes_string = '';
			if (days_until || hours_until || minutes_until) minutes_string = pad(minutes_until, 2) + ':';

			seconds_string = pad(seconds_until, 2);
			clock_string = "Next Worship Event: " + days_string + hours_string + minutes_string + seconds_string;
		}
	}

	$('clock_status').html(clock_string);
}

function stop_clock()
{
	clearTimeout(clock_timer);
}

function stop_slideshow()
{
	clearTimeout(slide_timer);
}

function do_final_countdown(milliseconds)
{
	seconds = Math.floor(milliseconds / 1000);
	$('overlay').show();
	$('overlay').html(seconds + "<small>seconds left</small>");
	r = Math.floor((Math.random()*128)+1);
	g = Math.floor((Math.random()*128)+1);
	b = Math.floor((Math.random()*128)+1);
	$('overlay').css('background-color', 'rgb('+ r + ', '+ g + ', ' + b + ')');
}

function switch_to_video(url)
{
	// load the video, wait and play
	$('#player').attr('type','rtmp/mp4');
	$('#player').attr('src',url);
	$('#player').on('canplaythrough', function(e){
		stop_slideshow()
		$(this).fadeIn();
		$(this).get(0).play();
		// stop_clock()
	});
	$('#player').on('ended', function(e){$(this).fadeOut();do_clock();do_slideshow();})
}

function check_livestream()
{
	// var stream_url = 'https://192.168.10.50:8888/mobile/index.m3u8';
	// $.get(stream_url, function(data){
	// 	// if we got here there is an active stream!
	// 	switch_to_video(stream_url);
	// })
}

$(window).load(function(){
	do_clock();
	do_slideshow();
	throbber.stop();
	$('placeholder').hide();
})

$(document).ready(function(){
	throbber = new Throbber('throbber');
	throbber.options.spokes = 20;
	throbber.options.speedMS = 200;
	throbber.options.style = 'balls';
	throbber.throb();
})

function Throbber(containerId) {
  this.options = {
    speedMS: 100,
    center: 4,
    thickness: 3,
    spokes:8,
    color: [0,0,0],
    style: "line" //set to "balls" for a different style of throbber
  };
  this.t = document.getElementById(containerId);
  this.c = document.createElement('canvas');
  this.c.width = this.t.offsetWidth;
  this.c.height = this.t.offsetHeight;
  this.t.appendChild(this.c);
  this.throb = function() {
    var ctx = this.c.getContext("2d");
    ctx.translate(this.c.width/2, this.c.height/2);
    var w = Math.floor(Math.min(this.c.width,this.c.height)/2);
    var self = this;
    var o = self.options;
    var draw = function() {
      ctx.clearRect(-self.c.width/2,-self.c.height/2,self.c.width,self.c.height)
      ctx.restore();
      ctx.shadowOffsetX = ctx.shadowOffsetY = 1;
        ctx.shadowBlur = 2;
        ctx.shadowColor = "rgba(220, 220, 220, 0.5)";
        for (var i = 0; i < o.spokes; i++) {
        r = 255-Math.floor((255-o.color[0]) / o.spokes * i);
        g = 255-Math.floor((255-o.color[1]) / o.spokes * i);
        b = 255-Math.floor((255-o.color[2]) / o.spokes * i);
          ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
        if(o.style == "balls") {
          ctx.beginPath();
          ctx.moveTo(w,0)
          ctx.arc(w-Math.floor(Math.PI*2*w/o.spokes/3),0,Math.floor(Math.PI*2*w/o.spokes/3),0,Math.PI*2,true);
          ctx.fill();
        } else { ctx.fillRect(o.center, -Math.floor(o.thickness/2), w-o.center, o.thickness); }
        ctx.rotate(Math.PI/(o.spokes/2))
        if(i == 0) { ctx.save(); }
      }
    };
    draw();
    this.timer = setInterval(draw,this.options.speedMS);
  };
  this.stop = function() {
    clearInterval(this.timer);
    this.c.getContext("2d").clearRect(-this.c.width/2,-this.c.height/2,this.c.width,this.c.height)
  };
};

/* CODE TO HANDLE LAFAYETTECC.ORG/SERVE DATA */
var show_serve_images = 0;
var serve_data = {};
serve_data.hours = 0;
serve_data.dollars = 0;
serve_data.photos = [];

function get_serve_data()
{
	if (show_serve_images)
	{
		$.get('//lafayettecc.org/serve/?json=1', function(data){
			serve_data = data;

			// remove all serve_slides
			$('.serve_slide').remove();

			// add grabbed photos to slideshow
			for (i in serve_data.photos)
			{
				serve_data.photos[i].url = '/serve/' + serve_data.photos[i].url;
				html = '' +
					'<slide class="serve_slide" style="display:none;background-position:center;background-size:cover;background-image:url(' + serve_data.photos[i].url + ');">' +
					'<div class="serve_caption"><div class="stats">Hours Served: ' + serve_data.hours + ' || Dollars Donated: $' + serve_data.dollars_formatted + '</div><h2>#LCCSERVE</h2>' + serve_data.photos[i].description + '</div>' +
					'</slide>';
				$('slideshow').append(html);
			}
		})
	}
}
$(window).load(get_serve_data)


function do_resize()
{
	var w = window.innerWidth;
	var s = w / 1920;
	$('window').css('transform','scale3d(' + s + ',' + s + ',' + s + ')');
	$('placeholder').css('transform','scale3d(' + s + ',' + s + ',' + s + ')');
}
$(window).load(do_resize)
$(window).resize(do_resize)
</script>
<script src="https://vjs.zencdn.net/6.2.4/video.js"></script>
</html>
