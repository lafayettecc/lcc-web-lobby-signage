<?php include 'lib.php'; ?>
<?php
if (isset($_GET['fancy'])) $fancy = 1;
else $fancy=0;
?>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="lib.js"></script>
<link href='http://fonts.googleapis.com/css?family=Rambla:400,700,400italic,700italic|Julius+Sans+One|Oregano:400,400italic' rel='stylesheet' type='text/css'>
<style>
	* {margin:0;padding:0;border:0;}
	body {width: 1920px; height: 1080px;margin:0; padding:0; background-color: black;position:relative;overflow:hidden;}
	window {width: 1920px; height: 1080px;margin:0; padding:0; background-color: red;position:absolute;}
	content-full {width: 1920px; height: 1080px; background-color: blue;position:absolute;top:0; left:0;}
	content-left {width: 1440px; height: 1080px; background-color: white;position:absolute;top:0;left:0;}
	stoplight {width: 480px; border-left:10px solid black; height: 1080px; background-color: #dda;position:absolute;right:0px;top:0;}
	light, red, yellow, green
	{
		width: 270px;
		height: 270px;
		border-radius: 150px;
		border:15px solid black;
		background-color: black;
		position: absolute;
		left:83px;
	}
	/*light {width:198px;height:198px;}*/
	light {height:290px;}
	#light-1 {top: 30px;}
	#light-2 {top: 375px;}
	#light-3 {top: 720px;}
	red {background-color: #f00;top: 45px;}
	yellow {background-color: #ff0;top: 390px;}
	green {background-color: #10f010;;top: 735px;}
	.light {opacity: 0.1;}

	/* with gradients */
	red{background-image: -webkit-linear-gradient(bottom, rgb(140,10,10) 4%, rgb(255,0,0) 52%, rgb(217,7,7) 76%);}
	yellow{background-image: -webkit-linear-gradient(bottom, rgb(163,155,3) 4%, rgb(255,242,0) 52%, rgb(240,228,0) 76%);}
	green{background-image: -webkit-linear-gradient(bottom, rgb(18,163,18) 4%, rgb(16,240,16) 52%, rgb(9,224,9) 76%); }
	ticker {width:1440px; height:100px;position:absolute;bottom:0;left:0;background:navy;opacity:.5;}
	room_status {position:absolute;right:15px;text-align:right;color:white;font-family:Oregano, serif;font-size:44pt;line-height:72pt;min-width:1000px;}
	clock {position:absolute;left:15px;text-align:left;color:white;font-family:'Rambla', Impact, sans-serif;font-weight:700;font-size: 58pt;}
	slideshow {position:absolute;top:0;left:0;width:100%;height:1080px;overflow:hidden;}
	slide {display:none;position:absolute; top:0;left:0;min-width:100%;min-height:1080px;overflow:hidden;}
	slide img {width:100%;min-height:1080px;}
	progress {position:absolute; bottom:0;left:0;width:100%;height:5px;color:#fff; opacity:.5}
</style>
</head>
<body>
<window>
<content-full>content</content-full>
<content-left>
<slideshow>

	<?php foreach (get_ads('kidopolis') as $ad): ?>
	<slide><img src="<?php echo $ad; ?>" /></slide>
	<?php endforeach; ?>

</slideshow>
</content-left>
<stoplight>
<light id="light-1">&nbsp;</light>
<light id="light-2">&nbsp;</light>
<light id="light-3">&nbsp;</light>
<red class="off light">&nbsp;</red>
<yellow class="off light">&nbsp;</yellow>
<green class="off light">&nbsp;</green>
</stoplight>
<ticker>
<progress>&nbsp;</progress>
<clock>9:00 am</clock>
<room_status>Kidopolis rooms will open in 30 minutes.</room_status>
</ticker>
</window>
</body>
<script>
var today=new Date();
today.setHours(0)
today.setMinutes(0)
today.setSeconds(0);

<?php if ($_GET['test'] == 1): ?>

page_load_time = new Date();
page_load_ms = page_load_time.getTime()

<?php endif; ?>

var opacity=1;
var current_color='none';
var slide_interval = 12;
var current_slide=0;
var slideshow_started=0;
var service_times=new Array(
	new Date(today.getYear(), today.getMonth(), today.getDate(), 9, 0, 0, 0),
	new Date(today.getYear(), today.getMonth(), today.getDate(), 10, 45, 0, 0)
);

var do_fade = <?php if ($fancy) echo 1; else echo 0; ?>;

// set jquery animation speed
jQuery.fx.interval = <?php if ($fancy) echo 10; else echo 100; ?>;


function pulse_light()
{
	$('.on').fadeTo(500,.2).fadeTo(500,1);
	setTimeout(pulse_light, 2000);
}

function turn_on(light)
{
	if (current_color == light) return;
	$('.light').removeClass('on');
	$('.light').addClass('off');
	$(light).addClass('on');
	$(light).removeClass('off');
	$('.on').fadeTo(1000,1);
	$('.off').fadeTo(1000,0.1);
	current_color = light;
}
function adjust_opacity()
{
	opacity = (opacity + 1) % 10;
	$('.off').fadeTo(1000, opacity / 10);
	$('#adjust').html(opacity);
}
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function check_lights(date)
{
	s = date.getSeconds()
	i = Math.floor(s/10) %3;
	colors = new Array('red','yellow','green');
	if (colors[i] != current_color) turn_on(colors[i]);
}
function do_slideshow()
{
	if ( ! slideshow_started )
	{
		$($('slide').get(0)).show();
		slideshow_started = 1;
		current_slide = 0;
	}
	else
	{
		slides = $('slide');
		if(do_fade) $(slides.get(current_slide)).fadeOut(2000);
		else $(slides.get(current_slide)).hide();
		current_slide+=1;
		if (current_slide == slides.length) current_slide=0;
		if(do_fade) $(slides.get(current_slide)).fadeIn(2000);
		else $(slides.get(current_slide)).show();
	}
	window.setTimeout(do_slideshow, slide_interval*1000);
}

function do_clock()
{
	// first, we compute the clock and show it
	var d = new Date();

	<?php if ($_GET['test']==1): ?>

	elapsed_time = d.getTime() - page_load_ms;
	fake_clock = new Date();
	fake_clock.setHours(8);
	fake_clock.setMinutes(26);
	d.setTime( (fake_clock.getTime() + elapsed_time * 60 ));

	<?php endif; ?>

	clock=get_clock(d);
	$('clock').html(clock);

	// next we compute the room open or closed status
	time_diffs = new Array();
	for (i in service_times)
	{
		service_minutes = service_times[i].getHours() * 60 + service_times[i].getMinutes()
		now_minutes = d.getHours() * 60 + d.getMinutes()
		diff = service_minutes - now_minutes
		room_status = new Array()
		room_status = {light: 'red', text: 'Kidopolis rooms will reopen in ' + (diff-15) + ' minutes.'};
		if (diff < -80) room_status = {light: 'red', text: 'Kidopolis is now closed.'}
		else if (diff <= -70) room_status = {light: 'yellow', text: 'Watch out for heavy traffic ahead.'}
		else if (diff <= -20) room_status = {light: 'red', text: 'Kidopolis is closed for check-ins.'};
		else if (diff <= 15) room_status = {light: 'green', text: 'Kidopolis rooms are open for check-in.'};
		// definitely before or during this service, so we break and don't consider the next service time
		if (diff >= -80) break;
	}

	turn_on(room_status.light);
	$('room_status').html(room_status.text);

	window.setTimeout(do_clock, 1000);
}


$(document).ready(function(){
	//load_ads();
	do_clock();
	do_slideshow();
})
</script>

</html>
