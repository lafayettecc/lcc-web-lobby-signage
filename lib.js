function get_clock(d)
{
	// d is a datetime object
	h = d.getHours();
	m = d.getMinutes();
	s = d.getSeconds();
	ampm = 'am';
	if (h > 12)
	{
		ampm = 'pm';
		h = h-12;
	}
	if (h == 12) ampm = 'pm';
	if (h == 0) h = 12;
	//h = pad(h,2);
	m = pad(m,2);
	s = pad(s,2);
	//clock = h + ':' + m + ':' + s + ' ' + ampm;
	clock = h + ':' + m + ' ' + ampm;
	return clock;
}