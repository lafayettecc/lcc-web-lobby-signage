<?php
require_once('conf.php');

global $config;
function get_all_ads($ad_type = 'kidopolis')
{
	if ($ad_type != '') $path = 'ads/' . $ad_type;
	else $path = 'ads';
	if (!file_exists($path)) mkdir($path, 0770, TRUE);
	$h = opendir($path);
	$ads = Array();
	while (false !== ($entry = readdir($h)))
	{
		if ($entry == '.' or $entry == '..') continue;
		if (substr($entry,0,1) == '.') continue;
		if (is_dir($path . '/' . $entry)) continue;
		$ads[] = $path . '/' . $entry;
    }
  sort($ads);
	return $ads;

}
function get_ads($sign)
{
	global $config;
	return $config['signs'][$sign]['images'];
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
function read_config()
{
	global $config;
	$cdb = 'conf.db';
	$assoc = TRUE;
	if (file_exists($cdb)) $config = json_decode(file_get_contents('conf.db'), $assoc);
	else $config = array(
		'signs' => array(
			'main' => array(
				'name'=>'main',
				'images'=>array()
			)
		)
	);
	//clean_config();
	if (file_exists('config_updated')) unlink('config_updated');
}
function write_config()
{
	global $config;
	clean_config();
	file_put_contents('conf.db', json_encode($config));
	touch('config_updated');
}
function clean_config()
{
	global $config;
	// check for valid signs
	foreach ($config['signs'] as $sign_name=>$sign )
	{
		foreach ( $sign['images'] as $index=>$ad )
		{
			if (! file_exists($ad) )
			{
				remove_from_sign($sign_name, $ad);
			}
		}
	}
}

function add_to_sign($sign_name, $image_url)
{
	global $config;
	$sign = $config['signs'][$sign_name];
	if (in_array($image_url, $sign['images'])) return;
	else $sign['images'][] = $image_url;
	$config['signs'][$sign_name] = $sign;
	write_config();
}

function remove_from_sign($sign_name, $image_url)
{
	global $config;
	$sign = $config['signs'][$sign_name];
	if (! in_array($image_url, $sign['images'])) return;
	else
	{
		$image_index = array_search($image_url, $sign['images']);
		unset($sign['images'][$image_index]);
	}
	$config['signs'][$sign_name] = $sign;
	write_config();
}

function clear_sign($sign_name)
{
	global $config;
	$config['signs'][$sign_name]['images'] = Array();
}
function _debug($s)
{
	print "<pre>";
	print_r ($s);
	print "</pre>";
}

// INITIALIZATION CODE
/* AUTHENTICATION */
require('auth.php');


read_config();
?>
