<?php
global $config;
include "lib.php";

session_start();
authenticate();

$ads = get_all_ads('');

if (isset($_POST['newconfig']))
{
	$config = $_POST['newconfig'];
	write_config();
	// _debug($config);
	die();
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Jeff's Custom Signage Administration</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>

	<!-- jquery -->
	<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
	<link rel="stylesheet" href="//code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
	<script src="//code.jquery.com/jquery-1.8.2.min.js"></script>
	<script src="//code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>
	<!-- end jquery -->
	
	<script src="conf.js"></script>

	<style type="text/css">

	body {margin:0;font-size:9pt;background-color:#111 !important;font-family:'Open Sans';}
	#sidebar {position:absolute;top:10%;right:0;height:90%;width: 33%; float:right;border-left:1px solid #aaa; padding:0 20px 0 20px;box-sizing:border-box;font-size:12pt;overflow:auto;}
	.ad_block {border: 2px solid rgba(200,200,255,.8);padding:16px;border-radius:20px;}
	.ad_block {margin-bottom:60px;position:relative;}
	.caption {font-size:.8em;opacity:.6;}

	#content {position:absolute;top:10%;height:90%;padding:20px;background: #ddd;color:#222;margin-right:33%;box-sizing:border-box;overflow:auto;}

	#header {z-index:99;position:absolute;top:0;width:100%;height:10%;background-color:black;padding:10px;box-sizing:border-box;}
	#page-container {position:absolute;bottom:0;top:0;height:60% !important;overflow:auto;}


	.ui-collapsible-heading, .ui-btn-inner, .ui-button-text {font-size:1.2em;}
	input.ui-input-text, textarea.ui-input-text {font-size: 2em;}

	.ui-collapsible-content {position:relative;overflow:hidden;}
	.grid { width: 100%; padding:0;position:relative;}
	.grid .block {width:25%;margin:0;padding:0;position:relative;float:left;}
	.grid .block .selected {position: absolute; top:20px; text-align:center; padding: 10px; font-weight:bold; background: rgba(50,50,50,.8);text-shadow:none;color:white;display:none;}

	.clear { clear: both; }

	#sidebar img,
	#content img {max-width:100%;box-sizing:border-box;padding:6px; background:white;box-shadow:2px 2px 4px #aaa;border-radius:2px;}
	#sidebar img {max-width: 98%;}
	
	a.sign-link {display:inline-block;padding:5px 10px;border:1px solid;font-size:.7em;border-radius:3px;cursor:pointer;}
	a.sign-link:hover{background:white;}

	@media all and (min-width: 1000px){

		body {font-size:14pt;}
		.ui-collapsible-heading, .ui-btn-inner, .ui-button-text {font-size:1.0em;}
		input.ui-input-text, textarea.ui-input-text {font-size: 2em;}
	}
	@media all and (-webkit-min-device-pixel-ratio: 1.5){
		.ui-collapsible-heading, .ui-btn-inner, .ui-button-text {font-size:1.2em;}
		input.ui-input-text, textarea.ui-input-text {font-size: 2em;}
	}

	.button {text-align: center; position:relative; top:10px; margin-bottom:10px; padding: 8px; background:#ddd; border-radius:6px;}
	.button a {text-decoration: none;}

	</style>

	<script>

	// these are first grabbed by php, but subsequently, they will be updated by ajax calls
	var config = <?php print json_encode($config); ?>;
	var ads = <?php print json_encode($ads); ?>;

	fix_config();

	function fix_config()
	{
		// json_encode returns all associative arrays as objects and sometimes the sign images
		// get converted to associative arrays with numerical string indices, so we need
		// to fix that here.
		for (sign_name in config.signs)
		{
			config.signs[sign_name].images = obj_to_array(config.signs[sign_name].images)
		}
	}

	function do_refresh()
	{
		populateSidebar();
		populateSigns();
	}

	function update_config()
	{
		$.post('ajax.php?action=write_config', {newconfig: config}, function(data){});
	}
	
	function add_to_sign(sign, imgurl)
	{
		index = config.signs[sign].images.indexOf(imgurl);
		if (index != -1) return;
		else config.signs[sign].images.push(imgurl);
		update_config();
		do_refresh();
	}

	function remove_from_sign(sign_name, imgurl)
	{
		index = config.signs[sign_name].images.indexOf(imgurl);
		if (index == -1 ) return;
		else config.signs[sign_name].images.splice(index,1);
		update_config();
		do_refresh();
	}

	function check_this_box(id)
	{
		//alert(id);
		checkbox = $("#" + id);
		details = id.split('_');
		imgkey = details[1];
		sign = details[3];
		imgurl = ads[imgkey];
		if (checkbox.attr('checked')) add_to_sign(sign, imgurl);
		else remove_from_sign(sign, imgurl);
	}
	function refreshPage()
	{
		 jQuery.mobile.changePage(window.location.href, {
			  allowSamePageTransition: true,
			  transition: 'none',
			  reloadPage: true
		 });
	}

	function obj_to_array(obj)
	{
		var newArray = []
		for (var key in obj)
		{
			item = obj[key]
			if (typeof(item) === 'object' && ! Array.isArray(item))
			{
				newArray.push(obj_to_array(item))
			}
			else
			{
				newArray.push(item)
			}
		}
		return newArray;
	}

	function in_array(obj, arr)
	{
		if (! Array.isArray(arr)) arr = obj_to_array(arr)
		index = arr.indexOf(obj)
		if ( index == -1 ) return 0;
		else return 1;
	}

	function populateSidebar()
	{
		sidebar = ''
		for (i in ads)
		{
			ad = ads[i]
			html = '<div class="ad_block">\n';
			html += '\n<span class="caption">Select which signs will display this image:</span>';
			html += '\n<img src="' + ad + '" />';
			html += '\n<input type="hidden" id="ad_' + i + '" name="ad_' + i + '" value="' + ad + '" />';
			for (name in config.signs)
			{
				sign = config.signs[name]
				if ( in_array(ad, sign.images) ) checked = 'checked="checked"';
				else checked='';
				html += '\n<label>';
				html += '<input type="checkbox" name="assign_' + i + '_to_' + name + '" id="assign_' + i + '_to_' + name + '" value="' + name + '" onClick="check_this_box( \'assign_' + i + '_to_' + name + '\' );" ' + checked + ' />';
				html += name;
				html += '</label>';
			}
			html += '</div>';
			sidebar += html;
		}
		$('#sidebar-ads').html(sidebar);
		$('#sidebar-ads').trigger('create');
	}

	function populateSigns()
	{
		html = ''
		for (name in config.signs)
		{
			sign = config.signs[name];
			stream_data = sign.stream_data ? sign.stream_data : {stream_url: "", stream_active_url: ""};
			html += '<div data-role="collapsible" data-theme="b" data-content-theme="b" data-collapsed="false">';
			html += '<h3>' + name + '</h3>';
			html += '<div class="grid">';
			for (index in sign.images)
			{
				image = sign.images[index];
				html += '<div class="block"><a href="#" title="CLICK TO REMOVE THIS AD FROM THIS SIGN" onclick="remove_from_sign(\'' + name + '\', \'' + image + '\'); return false;"><img src="' + image + '" /></a><div class="selected">SELECTED</div></div>';
			}
			html += '</div> <!-- grid -->';
			html += '<div class="clear button">';
			html += '<a class="sign-link" href="'+home_url+'?sign=' + name + '" target="SIGN_WINDOW">View Sign</a>';
			html += ' <a class="sign-link" href="'+home_url+'?fancy=1&effect=fade&sign=' + name + '" target="SIGN_WINDOW">View Sign (Fade Effect)</a>';
			html += ' <a class="sign-link" href="'+home_url+'?fancy=1&effect=slide&sign=' + name + '" target="SIGN_WINDOW">View Sign (Slide Effect)</a>';
			html += ' <a class="sign-link" href="'+home_url+'?sign=' + name + '&test=1" target="SIGN_WINDOW">View Sign in Test Mode</a>';
			html += '</div>';
			html += '<div class="clear button">';
			html += '<input class="form-input" data-sign="'+name+'" name="stream_url" placeholder="stream_url" value="'+stream_data.stream_url+'" />';
			html += '</div>';
			html += '<div class="clear button">';
			html += '<input class="form-input" data-sign="'+name+'" name="stream_active_url" placeholder="stream_active_url" value="'+stream_data.stream_active_url+'" />';
			html += '</div>';
			html += '</div><!-- collapsible -->';
		}
		$('#signs-container').html(html);
		$('#signs-container').trigger('create');
		$('.form-input').on('change',function(){
			var sign_name = $(this).attr('data-sign');
			var stream_name = $(this).attr('name');
			var url = $(this).val();
			if (! config.signs[sign_name]['stream_data'] )
				config.signs[sign_name]['stream_data'] = {stream_url: "", stream_active_url: ""};
			config.signs[sign_name]['stream_data'][stream_name] = url;
			update_config();
		});
	}
	</script>

</head>
<body>
<form method="POST" action="admin.php" data-ajax="false" onsubmit="return false;" >
<div data-role="page" id="page-container">
	<div id="header">
	<img src="//lafayettecc.org/images/logo-ball-on-black.png" />
	</div>

	<div id="sidebar">
	<h2>Available ads</h2>

	<div id="sidebar-ads">

	sidebar goes here

	</div>

	</div> <!-- sidebar -->
	<div id="content">
		<h1>Jeff's Custom Signage Administration</h1>
		<div data-role="collapsible-group" data-content-theme="b" id="signs-container">

		ads go here

		</div> <!-- collapsible-set -->
		<button onclick="do_refresh();">Refresh</button>
		<button onclick="document.location.href='jquery-upload/upload.html'">Click Here to Upload/Delete Images</button>

		<?php /*
		<h1>Upload New Images</h1>
		<!-- The file upload form used as target for the file upload widget -->
		<form id="fileupload" action="uploads.php" method="POST" enctype="multipart/form-data">
			<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
			<div class="row fileupload-buttonbar">
				<!-- The fileinput-button span is used to style the file input field as button -->
				<span class="fileinput-button">
					<span>Add files...</span>
				  	<input type="file" name="files[]" multiple>
				</span>
				<button type="submit" class="btn btn-primary start">
					<i class="icon-upload icon-white"></i>
					<span>Start upload</span>
				</button>
			</div>
		</form>
		*/ ?>


	</div> <!-- content -->
</div>
</form>
</body>
<script>
$.widget( "mobile.collapsiblegroup", $.mobile.collapsibleset, {
    options: {
        initSelector: ":jqmData(role='collapsible-group')"
    },
    _create: function() {
        $.mobile.collapsibleset.prototype._create.call(this);
        var $el = this.element;
        if (!$el.jqmData('collapsiblebound2')) {
            $el.jqmData('collapsiblebound2', true)
                .unbind('expand')
                .bind('expand', $._data($el.get(0), 'events')['collapse'][0]);
        }
    }
});

//auto self-init widgets
/*
$( document ).bind( "pagecreate create", function( e ) {
    $.mobile.collapsiblegroup.prototype.enhanceWithin( e.target );
});
*/
$(document).ready(function(){
do_refresh();
})
</script>

</html>
