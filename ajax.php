<?php
global $config;
include "lib.php";
header('Access-Control-Allow-Origin: *');
header('Cache-Control: no-cache, no-store, must-revalidate');

$response = 'error: requested action not found';
if (isset($_GET['action']))
{

	$A = $_GET['action'];

	if ($A == 'get_all_ads')	$response = get_all_ads('');

	if ($A == 'write_config')
	{
		if (isset($_POST['newconfig']))
		{
			$config = $_POST['newconfig'];
			write_config();
			$response = 'success';
		}
	}

	if ($A == 'get_config')
	{
		$response = $config;
	}

	if ($A == 'get_sign')
	{
		$sign = $_GET['sign'];
		$response = $config['signs'][$sign];
	}

	if ($A == 'add_sign')
	{
		$sign = $_GET['sign'];
		if (empty($config['signs'][$sign]))
		{
			$config['signs'][$sign] = array('name' => $sign, 'images' => array(), 'stream_data' => array());
			write_config();
			$response = true;
		}
		else
		{
			$response = false;
		}
	}
	

}
print json_encode($response);
?>